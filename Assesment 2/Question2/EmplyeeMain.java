import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class EmplyeeMain {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            System.out.println("Enter the number of Employees");
            int n = sc.nextInt();

            EmplyeeVo emp[] = new EmplyeeVo[n];
            EmplyeeBo bo =  new EmplyeeBo();

            System.out.println("Enter Employee Details: ");
            for(int i=0; i<n; i++) {
                emp[i] = new EmplyeeVo();

                System.out.print("ID: ");
                emp[i].setEmpid(sc.nextInt());

                System.out.print("Name: ");
                emp[i].setEmpname(sc.next());

                System.out.print("Annual Income: ");
                emp[i].setAnnualIncome(sc.nextInt());

                bo.calincomeTax(emp[i]);
            }

            System.out.println("\nEmployees List:");
            display(emp);

            System.out.println("\nEmployees after sorting Income Tax in descending order:");
            Collections.sort(Arrays.asList(emp), new EmployeeSort());
            display(emp);
        }

        public static void display(EmplyeeVo[] emp) {
            for(EmplyeeVo e: emp) {
                System.out.println(e);
            }
        }
}

