import java.util.Objects;

public class EmplyeeVo {

    private int empid;
    private String empname;
    private int annualIncome,incometax;

    public EmplyeeVo() {
    }

    public EmplyeeVo(int empid, String empname, int annualIncome, int incometax) {
        this.empid = empid;
        this.empname = empname;
        this.annualIncome = annualIncome;
        this.incometax = incometax;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public int getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(int annualIncome) {
        this.annualIncome = annualIncome;
    }

    public int getIncometax() {
        return incometax;
    }

    public void setIncometax(int incometax) {
        this.incometax = incometax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmplyeeVo emplyeeVo = (EmplyeeVo) o;
        return empid == emplyeeVo.empid &&
                annualIncome == emplyeeVo.annualIncome &&
                incometax == emplyeeVo.incometax &&
                Objects.equals(empname, emplyeeVo.empname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empid, empname, annualIncome, incometax);
    }


    @Override
    public String toString() {
        return "EmplyeeVo{" +
                "empid=" + empid +
                ", empname='" + empname + '\'' +
                ", annualIncome=" + annualIncome +
                ", incometax=" + incometax +
                '}';
    }
}
