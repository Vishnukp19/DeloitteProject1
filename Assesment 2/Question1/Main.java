import java.util.Scanner;

public class Main {
	static int num1, num2;
	static Scanner  sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		Arithmetic arr[] = {new Addition(), new Subtraction(), new Multiplication(), new Division()};
		read();
		
	System.out.println("Enter your choice: Addition(1),Subtraction(2),Multiplication(3),Division(4) ");
		Arithmetic x = arr[sc.nextInt()-1];
		x.read(num1,num2);
		x.calculate();
		x.display();

	}
	private static void read() {
		System.out.println("Enter First Number");
		num1 = sc.nextInt();
		
		System.out.println("Enter Second Number");
		num2 = sc.nextInt();
	}

}
