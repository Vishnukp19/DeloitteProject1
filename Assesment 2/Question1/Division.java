package q1;

public class Division extends Arithmetic {


	public void calculate() {
		try {
			this.num3 = this.num1 / this.num2;
		} catch (ArithmeticException e) {
			System.out.println("Number cannot be divided by 0");
		}
	}

}
